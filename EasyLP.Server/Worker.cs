using Baize.Core.Helpers;
using EasyLP.Common;
using HPSocket;
using HPSocket.Tcp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using Weave.Server;

namespace EasyLP.Server
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConfiguration _configuration;

        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        static QueueHelper<QueueEntity> _queue = new QueueHelper<QueueEntity>();

        private Dictionary<string, List<byte>> temp = new Dictionary<string, List<byte>>();
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ITcpPackServer _server = new TcpPackServer();
            _server.SocketBufferSize = 4096; // 4K
            _server.MaxPackSize = 4194303;     // 最大封包
            _server.PackHeaderFlag = 0x01;  // 包头标识, 要与客户端对应, 否则无法通信
            _server.Address = "0.0.0.0";
            _server.Port = 8555;
            _server.OnAccept += _server_OnAccept;
            _server.OnReceive += _server_OnReceive;
            _server.Start();
            Console.WriteLine($"Server Opened:{5555}");
            Console.WriteLine(_server.HasStarted);
            _queue.DealAction = x =>
             {
                 SendToClient(x.IServer, x.IntPtr, x.Msg);
             };
            await Task.CompletedTask;
        }

        /// <summary>
        /// 收到消息时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="connId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private HandleResult _server_OnReceive(IServer sender, IntPtr connId, byte[] data)
        {
            try
            {
                // 这里来的都是完整的包, 但是这里不做耗时操作, 仅把数据放入队列
                var packet = JsonConvert.DeserializeObject<PostDataEntity>(Encoding.UTF8.GetString(data));
                var result = HandleResult.Ok;
                sender.GetRemoteAddress(connId, out var ip, out var port);
                var key = $"{ip}_{port}";
                var admins = _configuration.GetSection("Admins").Get<List<string>>() ?? new List<string>();
                var errorMsg = "你已经被剥夺发布资格,如需更新请联系张强，邓振振，盛洪伟，请勿私自更新！";

                switch (packet.Type)
                {
                    case PacketType.C2S_Begin:
                        if (admins.Any() && !admins.Contains(packet.Account))
                            throw new Exception(errorMsg);

                        if (temp.ContainsKey(key))
                            temp[key] = new List<byte>();
                        else
                            temp.Add(key, new List<byte>());

                        _queue.Enqueue(new QueueEntity(sender, connId, $"From [{key}] start process..."));
                        Console.WriteLine($"Echo{packet.Msg}");
                        break;

                    case PacketType.C2S_AppendFile:
                        try
                        {
                            if (admins.Any() && !admins.Contains(packet.Account))
                                throw new Exception(errorMsg);

                            var tb = temp[key];
                            tb.AddRange(packet.FileBytes);
                            temp[key] = tb;
                            SendToClient(sender, connId, new PostDataEntity
                            {
                                Type = PacketType.C2S_AppendFile,
                                Receive = tb.Count
                            });
                            Console.WriteLine($"From[{key}] Receive File :{tb.Count}");
                        }
                        catch (Exception ex)
                        {
                            _queue.Enqueue(new QueueEntity(sender, connId, $"saved to file error:{ex.Message}"));
                        }

                        break;
                    case PacketType.C2S_FileOk:
                        if (admins.Any() && !admins.Contains(packet.Account))
                            throw new Exception(errorMsg);

                        var dateFolder = $"{DateTime.Now:yyyyMMdd}";
                        var basePath = Path.Combine("File", dateFolder);
                        if (!Directory.Exists(basePath))
                            Directory.CreateDirectory(basePath);

                        var zipFileName = $"{packet.CenterName}_{packet.Account}_{DateTime.Now:HHmmss.fff}.zip";

                        //文件
                        var save = Path.Combine(basePath, zipFileName);

                        //完整路径
                        var saveFullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, save);
                        //保存
                        File.WriteAllBytes(save, temp[key].ToArray());
                        temp.Clear();
                        //文件保存成功通知
                        _queue.Enqueue(new QueueEntity(sender, connId, $"saved to file:{saveFullPath}"));

                        if (packet.WindowsOS)
                            this.OpreationForWindows(packet, sender, connId, saveFullPath);
                        else
                            this.OpreationForLinux(packet, sender, connId, saveFullPath);
                        break;
                    case PacketType.C2S_ACKAPP:
                        var fs = new DirectoryInfo(packet.AppBasePath);
                        SendToClient(sender, connId, new PostDataEntity
                        {
                            Type = PacketType.S2C_ACKAPP,
                            Folder = fs.GetDirectories().Select(x => x.Name).ToArray()
                        });
                        break;
                    default:
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                _queue.Enqueue(new QueueEntity(sender, connId, $"Exception:{ ex.Message}"));
                return HandleResult.Ok;
            }

        }

        /// <summary>
        /// 接受时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="connId"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private HandleResult _server_OnAccept(IServer sender, IntPtr connId, IntPtr client)
        {
            // 获取客户端地址
            if (!sender.GetRemoteAddress(connId, out var ip, out var port))
            {
                return HandleResult.Error;
            }
            Console.WriteLine($"OnAccept({connId}), ip: {ip}, port: {port}");
            return HandleResult.Ok;
        }

        /// <summary>
        /// 发送客户端
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="connId"></param>
        /// <param name="msg"></param>
        private void SendToClient(IServer sender, IntPtr connId, string msg)
        {
            var json = JsonConvert.SerializeObject(new PostDataEntity { Type = PacketType.S2C_Echo, Msg = msg });
            var bytes = Encoding.UTF8.GetBytes(json);
            sender.Send(connId, bytes, bytes.Length);
        }

        /// <summary>
        /// 发送到客户端
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="connId"></param>
        /// <param name="postDataEntity"></param>
        private void SendToClient(IServer sender, IntPtr connId, PostDataEntity postDataEntity)
        {
            var json = JsonConvert.SerializeObject(postDataEntity);
            var bytes = Encoding.UTF8.GetBytes(json);
            sender.Send(connId, bytes, bytes.Length);
        }

        /// <summary>
        /// 保存回退文件
        /// </summary>
        private void BakFileForLinux(PostDataEntity packet, IServer sender, IntPtr connId, string workDir)
        {
            //备份文件地址
            var bakPath = Path.Combine($"/home/bak", $"{packet.CenterName}_{packet.Account}_{DateTime.Now:HHmmss.fff}");
            if (!Directory.Exists(bakPath))
                Directory.CreateDirectory(bakPath);

            //Move文件
            var cmd = $"cp -f {workDir}/* {bakPath}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
        }

        /// <summary>
        /// 在Linux上 更新操作
        /// </summary>
        private void OpreationForLinux(PostDataEntity packet, IServer sender, IntPtr connId, string saveFullPath)
        {

            var workDir = $"{packet.AppBasePath}/{packet.CenterName}";

            //备份文件
            this.BakFileForLinux(packet, sender, connId, workDir);

            //停止服务
            var stop = _configuration.GetSection("SupervisorStopCmd").Get<string>()?? "supervisorctl stop";
            var cmd = $"{stop} {packet.CenterName.Split('.')[0]}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //删除文件 先开启通配符功能
            cmd = "shopt -s  extglob";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //创建备份目录
            var backGuid = Guid.NewGuid().ToString();
            cmd = $"mkdir {packet.AppBasePath}/{backGuid}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //CP
            var ignoreList = _configuration.GetSection("IgnoreJson").Get<List<string>>();
            ignoreList.ForEach(file =>
            {
                cmd = $"cp {workDir}/{file} {packet.AppBasePath}/{backGuid}";
                CmdHelper.Linux(cmd);
                _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
                Console.WriteLine(cmd);
                Console.WriteLine($"IgnoreJson:{string.Join(',', ignoreList.ToArray())}");
            });

            //删除文件
            cmd = $"rm -rf {workDir}/*";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //解压文件
            cmd = $"unzip {saveFullPath} -d {workDir}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //Move文件
            cmd = $"mv -f {packet.AppBasePath}/{backGuid}/* {workDir}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //开始服务
            var start = _configuration.GetSection("SupervisorStartCmd").Get<string>()?? "supervisorctl start";
            cmd = $"{start} {packet.CenterName.Split('.')[0]}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //删除备份目录
            cmd = $"rm -rf {packet.AppBasePath}/{backGuid}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);

            //删除备份目录 文件
            cmd = $"rm -rf {saveFullPath}";
            CmdHelper.Linux(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //发布完成
            _queue.Enqueue(new QueueEntity(sender, connId, $"publish successful"));
        }

        /// <summary>
        /// 在Windows上 更新操作
        /// </summary>
        private void OpreationForWindows(PostDataEntity packet, IServer sender, IntPtr connId, string saveFullPath)
        {
            var workDir = $"{packet.AppBasePath}\\{packet.CenterName}";

            //备份文件
            this.BakFileForWindows(packet, sender, connId, workDir);

            //停止服务
            var cmd = $"sc stop {packet.CenterName}";
            var res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: 开始休息了"));
            Thread.Sleep(5000);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: 结束休息了"));

            //创建备份目录
            var backGuid = Guid.NewGuid().ToString();
            var beifen = $"{ packet.AppBasePath }\\{ backGuid}";

            cmd = $"mkdir {beifen}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //CP
            var ignoreList = _configuration.GetSection("IgnoreJson").Get<List<string>>();
            ignoreList.ForEach(file =>
            {
                if (File.Exists($"{workDir}\\{file}"))
                {
                    cmd = $"copy /y {workDir}\\{file} {beifen}";
                    res = CmdHelper.Windows(cmd);
                    _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
                    Console.WriteLine(cmd);
                    Console.WriteLine(res.Error);

                    //File.Copy($"{workDir}\\{file}", $"{beifen}\\{file}", true);

                    //_queue.Enqueue(new QueueEntity(sender, connId, $"Copy: {file}"));
                }
            });
            Console.WriteLine($"IgnoreJson:{string.Join(',', ignoreList.ToArray())}");


            //Directory.Delete(workDir, true);
            //_queue.Enqueue(new QueueEntity(sender, connId, $"Delete:{workDir}"));

            //删除文件
            cmd = $"rmdir /s/q {workDir}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);

            //解压文件
            if (ZipHelper.UnZip(saveFullPath, workDir))
                _queue.Enqueue(new QueueEntity(sender, connId, $"executed: Unzip Success"));
            else
                _queue.Enqueue(new QueueEntity(sender, connId, "找不到待解压文件"));

            //cmd = $"bz.exe x -y -o:{workDir} {saveFullPath}";
            //res = CmdHelper.Windows(cmd);
            //_queue.Enqueue(new QueueEntity(sender, connId, $"executed: Unzip"));
            //Console.WriteLine(cmd);
            //Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //Move文件
            cmd = $"move /y {beifen}\\* {workDir}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //开始服务
            cmd = $"sc start {packet.CenterName}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //Directory.Delete(beifen, true);
            //_queue.Enqueue(new QueueEntity(sender, connId, $"Delete:{beifen}"));

            //删除备份目录 配置文件
            cmd = $"rd /s/q {beifen}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "File"), true);
            //_queue.Enqueue(new QueueEntity(sender, connId, $"Delete:{Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "File")}"));

            //删除备份目录 文件
            cmd = $"rd /s/q {Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "File")}";
            res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));

            //发布完成
            _queue.Enqueue(new QueueEntity(sender, connId, $"publish successful"));
        }

        /// <summary>
        /// 保存回退文件
        /// </summary>
        private void BakFileForWindows(PostDataEntity packet, IServer sender, IntPtr connId, string workDir)
        {
            //备份文件地址
            var bakPath = Path.Combine($"{AppDomain.CurrentDomain.BaseDirectory[..3]}Bak", $"{packet.CenterName}_{packet.Account}_{DateTime.Now:HHmmss.fff}");
            if (!Directory.Exists(bakPath))
                Directory.CreateDirectory(bakPath);

            //Move文件
            var cmd = $"xcopy  {workDir}\\* {bakPath} /s /e";
            var res = CmdHelper.Windows(cmd);
            _queue.Enqueue(new QueueEntity(sender, connId, $"executed: {cmd}"));
            Console.WriteLine(cmd);
            Console.WriteLine(res.Error);
            //Console.WriteLine(JsonConvert.SerializeObject(res));
        }
    }
}
