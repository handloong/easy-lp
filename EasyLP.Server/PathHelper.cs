﻿#region License
/***
 * Copyright © 2018-2020, 张强 (943620963@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * without warranties or conditions of any kind, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
/****************************
* [Author] 张强
* [Date] 2018-06-24
* [Describe] 路径工具类
* **************************/
namespace Baize.Core.Helpers
{
    /// <summary>
    /// 路径工具类
    /// </summary>
    public static class PathHelper
    {
        #region 属性
        /// <summary>
        /// 当前操作系统路径分隔符，兼容Windows和Linux
        /// </summary>
        public static char CurrentOsDirectorySeparator = Path.DirectorySeparatorChar;
        #endregion

        /// <summary>
        /// 转换为当前操作系统的文件路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ConvertToCurrentOsPath(string path)
        {
            var sep = CurrentOsDirectorySeparator == '/' ? '\\' : '/';
            return path.Replace(sep, CurrentOsDirectorySeparator);
        }
    }
}
