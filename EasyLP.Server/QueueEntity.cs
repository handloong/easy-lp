﻿using HPSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyLP.Server
{
    public class QueueEntity
    {
        public QueueEntity()
        {
        }
        public QueueEntity(IServer server, IntPtr intPtr, string msg)
        {
            IServer = server;
            IntPtr = intPtr;
            Msg = msg;
        }
        public IServer IServer { get; set; }
        public IntPtr IntPtr { get; set; }
        public string Msg { get; set; }
    }
}
