using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace EasyLP.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("请使用管理员启动");
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    //协议
                    //services.AddSingleton<IProtocol<CommandLineMessage>, CommandLineProtocol>();
                    ////挂载服务逻辑
                    //services.AddSingleton<ISocketService<CommandLineMessage>, MyService>();
                    ////添加挂载的宿主服务
                    //services.AddTcpServer<CommandLineMessage>();

                    services.AddHostedService<Worker>();
                });
    }
}
