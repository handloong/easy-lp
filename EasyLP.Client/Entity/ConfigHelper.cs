﻿using dTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyLP.Client.Entity
{
    public class ConfigHelper
    {
        public static List<ConfigInfo> ConfigInfos
        {
            get
            {
                var saveJson = Path.Combine(Sys.CONFIG_PATH, "appsetting.json");
                if (File.Exists(saveJson))
                    return File.ReadAllText(saveJson, Encoding.UTF8).ToObject<List<ConfigInfo>>();
                else
                    return new List<ConfigInfo>();
            }
            set
            {
                File.WriteAllText(Path.Combine(Sys.CONFIG_PATH, "appsetting.json"), value.ToJson(), Encoding.UTF8);
            }
        }

        public static string Account
        {
            get
            {
                var saveJson = Path.Combine(Sys.CONFIG_PATH, "account.json");
                if (File.Exists(saveJson))
                    return File.ReadAllText(saveJson, Encoding.UTF8).ToObject<string>();
                else
                    return string.Empty;
            }
            set
            {
                File.WriteAllText(Path.Combine(Sys.CONFIG_PATH, "account.json"), value.ToJson(), Encoding.UTF8);
            }
        }
    }
}
