﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dTools;

namespace EasyLP.Client.Entity
{
    /// <summary>
    /// 服务信息
    /// </summary>
    public class ConfigInfo
    {
        public string Id { get; set; }
        public string ServerIp { get; set; }
        public int Port { get; set; }

        /// <summary>
        /// app根目录
        /// </summary>
        public string AppBasePath { get; set; }

        /// <summary>
        /// windows系统
        /// </summary>
        public bool WindowsOS { get; set; }

        public bool ShowCMd { get; set; }
        public string Desc { get; set; }
    }
}
