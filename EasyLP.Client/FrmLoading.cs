﻿using dTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dTools.Extensions;
using EasyLP.Client.Entity;
using System.Diagnostics;
using System.Threading;

namespace EasyLP.Client
{
    public partial class FrmLoading : Form
    {
        public FrmLoading()
        {
            InitializeComponent();
            this.ShowIcon = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLoading_Load(object sender, EventArgs e)
        {
            this.lv.Columns.Add("备注", 120, HorizontalAlignment.Left);
            this.lv.Columns.Add("IP", 120, HorizontalAlignment.Left);
            this.lv.Columns.Add("端口", 50, HorizontalAlignment.Left);
            this.lv.Columns.Add("账户", 50, HorizontalAlignment.Left);
            this.lv.Columns.Add("显示服务端命令", 100, HorizontalAlignment.Left);
            this.lv.Columns.Add("app根目录", 120, HorizontalAlignment.Left);
            this.lv.Columns.Add("windows系统", 100, HorizontalAlignment.Left);

            if (!Directory.Exists(Sys.CONFIG_PATH))
                Directory.CreateDirectory(Sys.CONFIG_PATH);
            LoadConfigs();

            Task.Run(() => { Bink(); });
        }

        /// <summary>
        /// 加载
        /// 服务器配置信息
        /// </summary>
        private void LoadConfigs()
        {
            var allConfigs = ConfigHelper.ConfigInfos.OrderBy(x=>x.Desc).ToList();

            this.lv.BeginUpdate();   //数据更新，UI暂时挂起，直到EndUpdate绘制控件，可以有效避免闪烁并大大提高加载速度
            lv.Items.Clear();
            allConfigs.ForEach(x =>
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = x.Desc;
                lvi.SubItems.Add(x.ServerIp);
                lvi.SubItems.Add(x.Port.ToString());
                lvi.SubItems.Add(ConfigHelper.Account);
                lvi.SubItems.Add(x.ShowCMd == true ? "显示" : "隐藏");
                lvi.SubItems.Add(x.AppBasePath);
                lvi.SubItems.Add(x.WindowsOS == true ? "Windows" : "Linux");

                this.lv.Items.Add(lvi);
                lvi.Tag = x;
            });
            this.lv.EndUpdate();  //结束数据处理，UI界面一次性绘制。
        }

        /// <summary>
        /// 新增 配置信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new FrmConfigForm(null, null, () => { LoadConfigs(); }).ShowDialog();
        }

        /// <summary>
        /// 编辑 配置信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var lvSs = this.lv.SelectedItems;
            if (lvSs.Count != 1)
            {
                MessageBox.Show("请先选择一个");
                return;
            }
            var configInfo = lvSs[0].Tag as ConfigInfo;
            new FrmConfigForm(configInfo.Id, configInfo, () => { LoadConfigs(); }).ShowDialog();
        }

        /// <summary>
        /// 进入服务器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (ConfigHelper.Account.IsEmpty())
            {
                MessageBox.Show("请先进入账户设置设置账户");
                return;
            }
            var lvSs = this.lv.SelectedItems;
            if (lvSs.Count != 1)
            {
                MessageBox.Show("请先选择一个");
                return;
            }
            var configInfo = lvSs[0].Tag as ConfigInfo;
            new FrmMain(configInfo).Show();
            this.Hide();
        }

        private void btnShare_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(Sys.CONFIG_PATH);
        }

        private void lblAccount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new FrmAccount(() => { LoadConfigs(); }).ShowDialog();
        }

        /// <summary>
        /// 单击进入服务器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lv_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            btnEnter_LinkClicked(sender, null);
        }

        /// <summary>
        /// 双击进入服务器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lv_DoubleClick(object sender, EventArgs e)
        {
            btnEnter_LinkClicked(sender, null);
        }

        private void btnReload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoadConfigs();
        }

        private int _blink = 0;

        /// <summary>
        /// 动效-更改图片
        /// </summary>
        private void Bink()
        {
            while (true)
            {
                if (_blink == 5)
                {
                    _blink = 0;
                }
                if (picLinux.InvokeRequired)
                    picLinux.BeginInvoke(new Action(() => { picLinux.Image = imageList.Images[_blink]; }));
                else
                    picLinux.Image = imageList.Images[_blink];
                _blink++;
                Thread.Sleep(300);
            }
        }
    }
}
