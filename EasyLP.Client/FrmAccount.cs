﻿using dTools;
using EasyLP.Client.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyLP.Client
{
    public partial class FrmAccount : Form
    {
        private readonly Action _action;

        public FrmAccount(Action action)
        {
            this.ShowIcon = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            _action = action;
        }

        private void FrmAccount_Load(object sender, EventArgs e)
        {
            txtAccount.Text = ConfigHelper.Account;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var account = txtAccount.Text;
            if (account.IsEmpty())
            {
                MessageBox.Show("必填项不能为空");
                return;
            }
            ConfigHelper.Account = account;
            _action?.Invoke();
            this.Close();
        }

        private void FrmAccount_Shown(object sender, EventArgs e)
        {
            txtAccount.Focus();
        }
    }
}
