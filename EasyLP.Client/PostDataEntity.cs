﻿using System;

namespace EasyLP.Common
{
    /// <summary>
    /// Post数据
    /// </summary>
    public class PostDataEntity
    {
        public string Account { get; set; }
        /// <summary>
        /// 封包类型
        /// </summary>
        public PacketType Type { get; set; }

        /// <summary>
        /// app根目录
        /// </summary>
        public string AppBasePath { get; set; }

        /// <summary>
        /// windows系统
        /// </summary>
        public bool WindowsOS { get; set; }

        /// <summary>
        /// 普通信息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 中心名称
        /// </summary>
        public string CenterName { get; set; }

        /// <summary>
        /// 文件数据
        /// </summary>
        public byte[] FileBytes { get; set; }

        /// <summary>
        /// 项目文件夹
        /// </summary>
        public string[] Folder { get; set; }

        /// <summary>
        /// 接收量
        /// </summary>
        public double Receive { get; set; }
    }

    /// <summary>
    /// 封包类型
    /// </summary>
    public enum PacketType
    {
        /// <summary>
        /// 开始
        /// </summary>
        C2S_Begin = 1,
        /// <summary>
        /// 文件
        /// </summary>
        C2S_AppendFile = 2,
        /// <summary>
        /// 文件OK
        /// </summary>
        C2S_FileOk = 3,

        /// <summary>
        /// 服务端返回客户端
        /// </summary>
        S2C_Echo = 4,

        /// <summary>
        /// 返回app下的文件夹
        /// </summary>
        S2C_ACKAPP = 5,

        /// <summary>
        /// 返回app下的文件夹
        /// </summary>
        C2S_ACKAPP = 6
    }
}
