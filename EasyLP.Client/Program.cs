﻿using dTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyLP.Client
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            //PushSapDataOnCAP();  
            var word = "10AB2aXXB2CD";

            //DCBXXaBA
            var reword = string.Join("",
                            word.Where(x => x >= 'A' && x <= 'Z' || x >= 'a' && x <= 'z')
                                .Reverse()
                    );



            var retval = string.Empty;


            var idx = 0;

            for (int i = 0; i < word.Length; i++)
            {
                var w = word[i];
                //int
                if (int.TryParse(w.ToString(), out int _))
                {
                    retval += w;
                }
                else
                {
                    retval += reword.ElementAt(idx);
                    idx++;
                }
            }
            Console.WriteLine(retval);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmLoading());
        }
    }
}
