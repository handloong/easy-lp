﻿
namespace EasyLP.Client
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.cboFolder = new System.Windows.Forms.ComboBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.lblPersent = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.菜单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsRestart = new System.Windows.Forms.ToolStripMenuItem();
            this.链接ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supervisorStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swaggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baizeHomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPublish = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblReceive = new System.Windows.Forms.Label();
            this.lblGetDir = new System.Windows.Forms.Label();
            this.启动新实例ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Location = new System.Drawing.Point(3, 137);
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(867, 364);
            this.txtLog.TabIndex = 11;
            this.txtLog.Text = "";
            // 
            // cboFolder
            // 
            this.cboFolder.FormattingEnabled = true;
            this.cboFolder.Location = new System.Drawing.Point(63, 6);
            this.cboFolder.Name = "cboFolder";
            this.cboFolder.Size = new System.Drawing.Size(242, 20);
            this.cboFolder.TabIndex = 13;
            // 
            // txtZip
            // 
            this.txtZip.AllowDrop = true;
            this.txtZip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtZip.Location = new System.Drawing.Point(3, 32);
            this.txtZip.Multiline = true;
            this.txtZip.Name = "txtZip";
            this.txtZip.ReadOnly = true;
            this.txtZip.Size = new System.Drawing.Size(864, 99);
            this.txtZip.TabIndex = 14;
            this.txtZip.Text = "③ZIP拖过来";
            this.txtZip.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtZip_DragDrop);
            this.txtZip.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtZip_DragEnter);
            // 
            // lblPersent
            // 
            this.lblPersent.BackColor = System.Drawing.Color.Transparent;
            this.lblPersent.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPersent.ForeColor = System.Drawing.Color.Blue;
            this.lblPersent.Location = new System.Drawing.Point(352, 7);
            this.lblPersent.Name = "lblPersent";
            this.lblPersent.Size = new System.Drawing.Size(123, 19);
            this.lblPersent.TabIndex = 18;
            this.lblPersent.Text = "发布进度 0%";
            this.lblPersent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.菜单ToolStripMenuItem,
            this.链接ToolStripMenuItem,
            this.btnConnect,
            this.btnPublish,
            this.启动新实例ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(884, 28);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 菜单ToolStripMenuItem
            // 
            this.菜单ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsRestart});
            this.菜单ToolStripMenuItem.Name = "菜单ToolStripMenuItem";
            this.菜单ToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.菜单ToolStripMenuItem.Text = "菜单";
            // 
            // tsRestart
            // 
            this.tsRestart.Name = "tsRestart";
            this.tsRestart.Size = new System.Drawing.Size(124, 22);
            this.tsRestart.Text = "重启软件";
            this.tsRestart.Click += new System.EventHandler(this.tsRestart_Click);
            // 
            // 链接ToolStripMenuItem
            // 
            this.链接ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supervisorStatusToolStripMenuItem,
            this.swaggerToolStripMenuItem,
            this.baizeHomeToolStripMenuItem});
            this.链接ToolStripMenuItem.Name = "链接ToolStripMenuItem";
            this.链接ToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.链接ToolStripMenuItem.Text = "链接";
            // 
            // supervisorStatusToolStripMenuItem
            // 
            this.supervisorStatusToolStripMenuItem.Name = "supervisorStatusToolStripMenuItem";
            this.supervisorStatusToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.supervisorStatusToolStripMenuItem.Text = "Supervisor Status";
            this.supervisorStatusToolStripMenuItem.Click += new System.EventHandler(this.supervisorStatusToolStripMenuItem_Click);
            // 
            // swaggerToolStripMenuItem
            // 
            this.swaggerToolStripMenuItem.Name = "swaggerToolStripMenuItem";
            this.swaggerToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.swaggerToolStripMenuItem.Text = "Swagger";
            this.swaggerToolStripMenuItem.Click += new System.EventHandler(this.swaggerToolStripMenuItem_Click);
            // 
            // baizeHomeToolStripMenuItem
            // 
            this.baizeHomeToolStripMenuItem.Name = "baizeHomeToolStripMenuItem";
            this.baizeHomeToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.baizeHomeToolStripMenuItem.Text = "BaizeHome";
            this.baizeHomeToolStripMenuItem.Click += new System.EventHandler(this.baizeHomeToolStripMenuItem_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Image = global::EasyLP.Client.Properties.Resources.btn_qx;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(112, 24);
            this.btnConnect.Text = "①连接服务器";
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnPublish
            // 
            this.btnPublish.Image = global::EasyLP.Client.Properties.Resources.btn;
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(76, 24);
            this.btnPublish.Text = "④发布";
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(884, 533);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblReceive);
            this.tabPage1.Controls.Add(this.txtLog);
            this.tabPage1.Controls.Add(this.lblGetDir);
            this.tabPage1.Controls.Add(this.lblPersent);
            this.tabPage1.Controls.Add(this.cboFolder);
            this.tabPage1.Controls.Add(this.txtZip);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(876, 507);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "首页";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblReceive
            // 
            this.lblReceive.BackColor = System.Drawing.Color.Transparent;
            this.lblReceive.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblReceive.ForeColor = System.Drawing.Color.Blue;
            this.lblReceive.Location = new System.Drawing.Point(458, 7);
            this.lblReceive.Name = "lblReceive";
            this.lblReceive.Size = new System.Drawing.Size(123, 19);
            this.lblReceive.TabIndex = 20;
            this.lblReceive.Text = "接收进度 0%";
            this.lblReceive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGetDir
            // 
            this.lblGetDir.AutoSize = true;
            this.lblGetDir.ForeColor = System.Drawing.Color.Blue;
            this.lblGetDir.Location = new System.Drawing.Point(4, 10);
            this.lblGetDir.Name = "lblGetDir";
            this.lblGetDir.Size = new System.Drawing.Size(53, 12);
            this.lblGetDir.TabIndex = 19;
            this.lblGetDir.Text = "②文件夹";
            this.lblGetDir.Click += new System.EventHandler(this.lblGetDir_Click);
            // 
            // 启动新实例ToolStripMenuItem
            // 
            this.启动新实例ToolStripMenuItem.Image = global::EasyLP.Client.Properties.Resources.Copy;
            this.启动新实例ToolStripMenuItem.Name = "启动新实例ToolStripMenuItem";
            this.启动新实例ToolStripMenuItem.Size = new System.Drawing.Size(100, 24);
            this.启动新实例ToolStripMenuItem.Text = "启动新实例";
            this.启动新实例ToolStripMenuItem.Click += new System.EventHandler(this.启动新实例ToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "白泽服务发布工具";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.ComboBox cboFolder;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label lblPersent;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 菜单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 链接ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supervisorStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swaggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsRestart;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStripMenuItem btnPublish;
        private System.Windows.Forms.ToolStripMenuItem btnConnect;
        private System.Windows.Forms.ToolStripMenuItem baizeHomeToolStripMenuItem;
        private System.Windows.Forms.Label lblGetDir;
        private System.Windows.Forms.Label lblReceive;
        private System.Windows.Forms.ToolStripMenuItem 启动新实例ToolStripMenuItem;
    }
}

