﻿
namespace EasyLP.Client
{
    partial class FrmLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLoading));
            this.lv = new System.Windows.Forms.ListView();
            this.btnCreate = new System.Windows.Forms.LinkLabel();
            this.btnEdit = new System.Windows.Forms.LinkLabel();
            this.lblAccount = new System.Windows.Forms.LinkLabel();
            this.btnEnter = new System.Windows.Forms.LinkLabel();
            this.btnShare = new System.Windows.Forms.LinkLabel();
            this.btnReload = new System.Windows.Forms.LinkLabel();
            this.picLinux = new System.Windows.Forms.PictureBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picLinux)).BeginInit();
            this.SuspendLayout();
            // 
            // lv
            // 
            this.lv.FullRowSelect = true;
            this.lv.HideSelection = false;
            this.lv.Location = new System.Drawing.Point(9, 10);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(692, 376);
            this.lv.TabIndex = 3;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lv_ItemCheck);
            this.lv.DoubleClick += new System.EventHandler(this.lv_DoubleClick);
            // 
            // btnCreate
            // 
            this.btnCreate.AutoSize = true;
            this.btnCreate.Location = new System.Drawing.Point(714, 219);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(29, 12);
            this.btnCreate.TabIndex = 7;
            this.btnCreate.TabStop = true;
            this.btnCreate.Text = "创建";
            this.btnCreate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnCreate_LinkClicked);
            // 
            // btnEdit
            // 
            this.btnEdit.AutoSize = true;
            this.btnEdit.Location = new System.Drawing.Point(714, 248);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(29, 12);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.TabStop = true;
            this.btnEdit.Text = "编辑";
            this.btnEdit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnEdit_LinkClicked);
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Location = new System.Drawing.Point(714, 306);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(53, 12);
            this.lblAccount.TabIndex = 9;
            this.lblAccount.TabStop = true;
            this.lblAccount.Text = "账户设置";
            this.lblAccount.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAccount_LinkClicked);
            // 
            // btnEnter
            // 
            this.btnEnter.AutoSize = true;
            this.btnEnter.Location = new System.Drawing.Point(714, 277);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(53, 12);
            this.btnEnter.TabIndex = 10;
            this.btnEnter.TabStop = true;
            this.btnEnter.Text = "进入系统";
            this.btnEnter.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnEnter_LinkClicked);
            // 
            // btnShare
            // 
            this.btnShare.AutoSize = true;
            this.btnShare.Location = new System.Drawing.Point(714, 364);
            this.btnShare.Name = "btnShare";
            this.btnShare.Size = new System.Drawing.Size(59, 12);
            this.btnShare.TabIndex = 11;
            this.btnShare.TabStop = true;
            this.btnShare.Text = "共享配置?";
            this.btnShare.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnShare_LinkClicked);
            // 
            // btnReload
            // 
            this.btnReload.AutoSize = true;
            this.btnReload.Location = new System.Drawing.Point(714, 335);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(53, 12);
            this.btnReload.TabIndex = 12;
            this.btnReload.TabStop = true;
            this.btnReload.Text = "重载配置";
            this.btnReload.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnReload_LinkClicked);
            // 
            // picLinux
            // 
            this.picLinux.Image = global::EasyLP.Client.Properties.Resources.linux_0;
            this.picLinux.Location = new System.Drawing.Point(709, 12);
            this.picLinux.Name = "picLinux";
            this.picLinux.Size = new System.Drawing.Size(200, 200);
            this.picLinux.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLinux.TabIndex = 13;
            this.picLinux.TabStop = false;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "linux_2.png");
            this.imageList.Images.SetKeyName(1, "linux_4.png");
            this.imageList.Images.SetKeyName(2, "linux_5.png");
            this.imageList.Images.SetKeyName(3, "linux_3.png");
            this.imageList.Images.SetKeyName(4, "linux_1.png");
            this.imageList.Images.SetKeyName(5, "linux_0.png");
            // 
            // FrmLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 400);
            this.Controls.Add(this.picLinux);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnShare);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.lblAccount);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnCreate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmLoading";
            this.Text = "白泽服务发布工具[EasyLP.Client]  -  发布一条龙 design by HandLoong";
            this.Load += new System.EventHandler(this.FrmLoading_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLinux)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.LinkLabel btnCreate;
        private System.Windows.Forms.LinkLabel btnEdit;
        private System.Windows.Forms.LinkLabel lblAccount;
        private System.Windows.Forms.LinkLabel btnEnter;
        private System.Windows.Forms.LinkLabel btnShare;
        private System.Windows.Forms.LinkLabel btnReload;
        private System.Windows.Forms.PictureBox picLinux;
        private System.Windows.Forms.ImageList imageList;
    }
}