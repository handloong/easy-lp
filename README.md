# EasyLP

#### 介绍
简化.Net Core 发布在Linux。 应用于没有自动发布的项目。
LP : Linux Publish

![发布中](https://images.gitee.com/uploads/images/2020/1127/092116_92476e88_1514386.png "123.PNG")

![发布完成](https://images.gitee.com/uploads/images/2020/1127/092128_98bb3911_1514386.png "456.PNG")

#### 软件架构
EasyLP.Server 运行于Linux服务端,基于.Net Core 5.0

EasyLP.Client 运行于开发者电脑,基于FW 4.5




#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


